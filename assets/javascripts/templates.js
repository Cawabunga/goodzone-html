(function (window, angular, undefined) {
  "use strict";

  var module = angular.module('app');
  module.run(['$templateCache', function ($templateCache) {

    $templateCache.put('addedToCart.html', '' +
      '<div class="text--center">' +
        '<div class="epsilon"><b>Товар добавлен в корзину!</b></div>' +
        '<a href="#" class="b-link">Перейти в корзину</a>' +
      '</div>'
    );


    $templateCache.put('popoverGray.html', '' +
      '<div class="popover popover--gray">' +
        '<div class="arrow"></div>' +
        '<h3 class="popover-title" ng-bind="title" ng-show="title"></h3>' +
        '<div class="popover-content" ng-bind="content"></div>' +
      '</div>'
    );

  }]);

}(window, window.angular));
