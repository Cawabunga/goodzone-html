(function (window, angular, undefined) {
  "use strict";

  var module = angular.module('app');
  module.controller('BaseController', [
    '$scope',
    '$q',
    function ($scope, $q) {

      $scope.toggleAuthPopover = function (event) {
        var elem = angular.element(event.currentTarget);
        elem.toggleClass('active');
      };


      /**
       * Cart methods
       */

      var cart;
      fetchCart().then(function (response) {
        cart = response;
      });

      $scope.addToCart = function (product) {
        if (!$scope.isInCart(product)) {
          console.log('Product added to cart', product);
          cart.push(product);
          addToCartRequest();
        }
      };

      $scope.isInCart = function (product) {
        var id = product.id ? product.id : product;
        return _.findWhere(cart, { id: id }) != null;
      };

      // Mocks
      function addToCartRequest() {
      }

      function fetchCart() {
        return $q.when([]);
      }


      /**
       * Typeahead Params
       */
      $scope.states = [
        {
          value: 'Great Wall Hover (Грейт Волл)',
          description: 'GALAXY (WGR) (03.1995 - ...)',
          image: '/assets/images/tmp/cart-item.png'
        },
        {
          value: 'Пушкин Александ Сергеевич',
          description: 'GALAXY (WGR) (03.1995 - Пушкин Александ Сергеевич)',
          image: '/assets/images/tmp/cart-item.png'
        }
      ];

      $scope.selectedState = '';
    }
  ]);

}(window, window.angular));
