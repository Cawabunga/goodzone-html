(function (window, angular, undefined) {
  "use strict";

  var module = angular.module('app');
  module.controller('ProductImagesPreviewController', [
    function () {
      var activeImage;

      this.setActiveImage = function (image) {
        activeImage = image;
      };

      this.getActiveImage = function () {
        return activeImage;
      };

    }
  ]);

}(window, window.angular));