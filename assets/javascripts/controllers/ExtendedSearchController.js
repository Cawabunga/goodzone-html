
angular.module('app')
.controller('ExtendedSearchController', [
  '$scope',
  function ($scope) {

    var data = [
      {
        id: 1, name: 'Acura',
        model: [
          {
            id: 1, name: 'модель (1)',
            submodel: [
              {
                id: 1, name: 'подмодель (1.1)',
                mod: [
                  { id: 1, name: 'модификация (1.1.1)' },
                  { id: 2, name: 'модификация (1.1.2)' },
                  { id: 3, name: 'модификация (1.1.3)' }
                ]
              },
              {
                id: 2, name: 'подмодель (1.2)',
                mod: [
                  { id: 1, name: 'модификация (1.2.1)' },
                  { id: 2, name: 'модификация (1.2.2)' },
                  { id: 3, name: 'модификация (1.2.3)' }
                ]
              }
            ]
          },
          {
            id: 2, name: 'модель (2)',
            submodel: [
              {
                id: 1, name: 'подмодель (2.1)',
                mod: [
                  { id: 1, name: 'модификация (2.1.1)' },
                  { id: 2, name: 'модификация (2.1.2)' },
                  { id: 3, name: 'модификация (2.1.3)' }
                ]
              },
              {
                id: 2, name: 'подмодель (2.2)',
                mod: [
                  { id: 1, name: 'модификация (2.2.1)' },
                  { id: 2, name: 'модификация (2.2.2)' },
                  { id: 3, name: 'модификация (2.2.3)' }
                ]
              }
            ]
          }
        ]
      },
      {
        id: 2, name: 'Alpha Rameo',
        model: [
          {
            id: 1, name: 'Alpha Rameo (1)',
            submodel: [
              {
                id: 1, name: 'Alpha Rameo (1.1)',
                mod: [
                  { id: 1, name: 'Alpha Rameo (1.1.1)' },
                  { id: 2, name: 'Alpha Rameo (1.1.2)' },
                  { id: 3, name: 'Alpha Rameo (1.1.3)' }
                ]
              },
              {
                id: 2, name: 'Alpha Rameo (1.2)',
                mod: [
                  { id: 1, name: 'Alpha Rameo (1.2.1)' },
                  { id: 2, name: 'Alpha Rameo (1.2.2)' },
                  { id: 3, name: 'Alpha Rameo (1.2.3)' }
                ]
              }
            ]
          },
          {
            id: 2, name: 'Alpha Rameo (2)',
            submodel: [
              {
                id: 1, name: 'Alpha Rameo (2.1)',
                mod: [
                  { id: 1, name: 'Alpha Rameo (2.1.1)' },
                  { id: 2, name: 'Alpha Rameo (2.1.2)' },
                  { id: 3, name: 'Alpha Rameo (2.1.3)' }
                ]
              },
              {
                id: 2, name: 'Alpha Rameo (2.2)',
                mod: [
                  { id: 1, name: 'Alpha Rameo (2.2.1)' },
                  { id: 2, name: 'Alpha Rameo (2.2.2)' },
                  { id: 3, name: 'Alpha Rameo (2.2.3)' }
                ]
              }
            ]
          }
        ]
      }
    ]

    var findMarkById = function (id) {
      return _.findWhere(data, { id: id })
    }

    var findModelById = function(markId, id) {
      var models = findMarkById(markId).model;
      return _.findWhere(models, { id: id })
    }

    var findSubmodelById = function(markId, modelId, id) {
      var submodels = findModelById(markId, modelId).submodel;
      return _.findWhere(submodels, { id: id })
    }

    var findModById = function(markId, modelId, submodelId, id) {
      var mods = findSubmodelById(markId, modelId, submodelId).mod;
      return _.findWhere(mods, { id: id })
    }

    $scope.data      = data;
    $scope.marks     = data;
    $scope.models    = {};
    $scope.submodels = {};
    $scope.mods      = {};

    $scope.searchData = {};

    $scope.setMark = function(mark) {
      $scope.searchData.mark = mark;
      $scope.models = mark.model;
      resetModel();
    }
    $scope.setModel = function(model) {
      $scope.searchData.model = model;
      $scope.submodels = model.submodel;
      resetSubmodel();
    }
    $scope.setSubmodel = function(submodel) {
      $scope.searchData.submodel = submodel;
      $scope.mods = submodel.mod;
      resetMod();
    }
    $scope.setMod = function(mod) {
      $scope.searchData.mod = mod;
    }
    $scope.findSubmodelById = findSubmodelById;

    var resetMod = function() {
      delete $scope.searchData.mod;
    }
    var resetSubmodel = function () {
      delete $scope.searchData.submodel;
      resetMod();
    }
    var resetModel = function () {
      delete $scope.searchData.model;
      resetSubmodel();
    }
    var resetMark = function () {
      delete $scope.searchData.mark;
      resetModel();
    }

  }
]);