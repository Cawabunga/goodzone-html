(function () {

  var module = angular.module('app');
  module.controller('FancySelectController', [
    '$scope',
    function ($scope) {
      $scope.opened = false;

      $scope.open = function () {
        $scope.opened = true;
      };
      $scope.close = function () {
        $scope.opened = false;
      };

      $scope.toggle = function () {
        $scope.opened = !$scope.opened;
      };

      $scope.setPlaceholder = function (title) {
        $scope.placeholder = title;
      };

      $scope.setItem = function (item, callback, closeFlag) {
        closeFlag = closeFlag === undefined;
        callback(item);
        $scope.setPlaceholder(item.name);
        if (closeFlag) $scope.close();
      }
    }
  ]);

}());
