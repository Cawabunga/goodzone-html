(function (window, angular, undefined) {
  'use strict';

  var app = angular.module('app', [
    'mgcrea.ngStrap.helpers.dimensions',
    'mgcrea.ngStrap.tooltip',
    'mgcrea.ngStrap.popover',
    'mgcrea.ngStrap.modal',
    'mgcrea.ngStrap.typeahead',
    'mgcrea.ngStrap.helpers.parseOptions'
  ]);

  app.run([
    '$log',
    function ($log) {
      $log.debug('App Kickstart');

      window.$('.collapse').collapse({
        toggle: false
      });
    }
  ]);


}(window, window.angular));