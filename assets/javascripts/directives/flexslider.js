angular.module('app')
  .directive('flexslider', [
    function() {
      return {
        restrict: 'A',
        scope: {
          settings: '&flexslider'
        },
        link: function (scope, elem, attrs) {
          var settings, defaults, flexSettings;

          settings = scope.$eval(scope.settings) || {};
          defaults = {
            animation: 'slide',
            slideshow: false,
            animationLoop: true
          }
          flexSettings = $.extend(defaults, settings);

          try {
            elem.flexslider(flexSettings);
          } catch (e) {
            console.error(e);
          }

        }
      }
    }
  ]);