(function (window, angular, undefined) {
  "use strict";

  var module = angular.module('app');
  module.directive('rmPopovers',
    function () {
      return {
        restrict: 'EA',
        link: function (scope, element, attrs) {

            angular.element('body').click(function (e) {
              var target = e.target;
              if (!element.find(target).length && _.indexOf(element, target) === -1) { // propagation stopping hack
                hideAllPopovers();
              }
            });

            var hideAllPopovers = function () {
              $('.popover').each(function(){
                var $this = $(this),
                  $scope = $this.scope();

                $scope.$hide();
              });
            }

        }
      }
    }
  );

}(window, window.angular));
