(function () {

  var module = angular.module('app');
  module.directive('fancySelect', [
    function () {
      return {
        restrict: 'EA',
        scope: true,
        link: function (scope, elem) {

          angular.element('body').click(function (e) {
            var target = e.target;
            if (!elem.find(target).length) { // propagation stopping hack
              scope.close();
              scope.$apply();
            }
          });

        },
        controller: 'FancySelectController'
      }
    }
  ]);

}());
